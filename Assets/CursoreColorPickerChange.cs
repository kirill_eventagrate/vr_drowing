﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursoreColorPickerChange : MonoBehaviour {

    

    void OnEnable()
    {
      
        ColorManager.OnClicked += ChangeColor;
    }


    void OnDisable()
    {
        ColorManager.OnClicked -= ChangeColor;
    }

    
    void ChangeColor()
    {

        gameObject.GetComponent<Renderer>().material.color = ColorManager.Instance.GetCurrentColor();
    }

}
