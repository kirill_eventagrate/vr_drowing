﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using UnityEngine;

public class PrintScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        PrintDrawing();
    }

    void PrintDrawing()
    {

        Debug.LogWarning("PrintDrawing");


        var installedPrinters = new List<string>();
        foreach (var p in PrinterSettings.InstalledPrinters)
        {
            installedPrinters.Add(p.ToString());
        }

#if UNITY_EDITOR
        var printerName = installedPrinters.Where(x => x.ToLower().Contains("epsone685b3")).FirstOrDefault();
#else
			var printerName = installedPrinters.Where(x => x.ToLower().Contains("hiti")).FirstOrDefault();
#endif

        if (printerName == null)
        {
            Debug.LogError("Printer with 'hiti' in name cannot be found.");
            return;
        }




        PrintDocument pd = new PrintDocument();
        pd.PrinterSettings.PrinterName = printerName;


        //var paperSize = new PaperSize("International postcard", 830, 1170);


        /*var resolution = new PrinterResolution();
        resolution.X = 150;
        resolution.Y = 150;*/

        //  pd.DefaultPageSettings.PrinterResolution = resolution;


        //  pd.DefaultPageSettings.Margins = new Margins(20, 20, 20, 3);
        //   pd.OriginAtMargins = true;

        /*pd.DefaultPageSettings.PaperSize = paperSize;
        pd.PrinterSettings.DefaultPageSettings.PaperSize = paperSize;
        */
        pd.DefaultPageSettings.Landscape = true;
        pd.PrinterSettings.DefaultPageSettings.Landscape = true;



        Debug.Log("Before DrawImageFromFile");

        pd.PrintPage += new PrintPageEventHandler(DrawImageFromFile);

        Debug.Log("before Printer");

        pd.Print();

        Debug.Log("End Printer");


    }

    void DrawImageFromFile(object o, PrintPageEventArgs e)
    {
        System.Drawing.Image i = System.Drawing.Image.FromFile("SavePicture\\screenShot-12-19-2019-16-30-4.png");
        System.Drawing.Rectangle m = e.MarginBounds;

        
        //e.Graphics.DrawImage(i, e.MarginBounds);
        // e.Graphics.DrawImage(i, 45,16,327,581);
        e.Graphics.DrawImage(i, 5, 5, 1123, 794);

    }

}
