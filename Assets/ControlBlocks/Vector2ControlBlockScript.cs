﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Vector2ControlBlockScript : MonoBehaviour {
	public Action<Vector2> OnChange = (_) => {};

	public bool autoInit = true;

	public ScalarControlBlockScript xScalarCB, yScalarCB;

	public string title, playerPrefsKey, oscAddress;
	public Text titleLabel;

	public Vector2 value;

	public Vector2 defaultValue = Vector2.zero;

	void Awake(){
		xScalarCB.autoInit = yScalarCB.autoInit = false;
	}

	void Start(){
		if (autoInit) {
			Init ();
		}
	}

	public void Init(){
		var allScalarCB = new List<ScalarControlBlockScript>{ 
			xScalarCB, yScalarCB
		};

		if (playerPrefsKey == "") {
			Debug.LogError ("playerPrefsKey should present");

			allScalarCB.ForEach (x => x.numInput.SetErrorColor());
			titleLabel.text = "no playerPrefs key";
			return;
		}

		if (title == "") {
			if (playerPrefsKey != "") {
				title = playerPrefsKey;
			}
		}

		titleLabel.text = title;


		xScalarCB.title = "x";
		xScalarCB.defaultValue = defaultValue.x;
		xScalarCB.playerPrefsKey = playerPrefsKey+"_x";
		xScalarCB.oscAddress = oscAddress+"/x";


		yScalarCB.title = "y";
		yScalarCB.defaultValue = defaultValue.y;
		yScalarCB.playerPrefsKey = playerPrefsKey+"_y";
		yScalarCB.oscAddress = oscAddress+"/y";


		// subscribe to change
		allScalarCB.ForEach(x => x.OnChange += OnScalarBlockChange);

		// now init
		allScalarCB.ForEach (x => x.Init ());
	}

	// EVENTS

	void OnScalarBlockChange(float val){

		//Debug.Log ("OnScalarBlockChange");

		value = new Vector2 (
			xScalarCB.value,
			yScalarCB.value
		);

		OnChange (value);
	}
}
