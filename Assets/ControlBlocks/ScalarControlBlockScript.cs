﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ScalarControlBlockScript : MonoBehaviour {


	public Action<float> OnChange = (_) => {};

	public bool autoInit = true;

	public string title = "";
	public string playerPrefsKey = "";
	public string oscAddress = "";

	public float defaultValue = 0;

	public Text titleLabel;
	public NumericalTouchControlScript numInput;

	public float value;


	void Awake(){
		numInput = GetComponentInChildren<NumericalTouchControlScript> ();
		numInput.OnChange += OnValueChange;
	}

	void Start(){
		if (autoInit) {
			Init ();
		}
	}

	public void Init() {
		LoadValueFromPlayerPrefs ();

		if (playerPrefsKey == "") {
			Debug.LogError ("playerPrefsKey should present");
			numInput.SetErrorColor ();
			titleLabel.text = "no playerPrefs key";
			return;
		}

		if (title == "") {
			if (playerPrefsKey != "") {
				title = playerPrefsKey;
			}
		}

		titleLabel.text = title;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	// METHODS

	void LoadValueFromPlayerPrefs(){
		if (PlayerPrefs.HasKey (playerPrefsKey)) {
			value = PlayerPrefs.GetFloat (playerPrefsKey);	
		} else {
			value = defaultValue;
			SaveValueToPlayerPrefs ();
		}

		numInput.SetCurrentValue (value);
	}

	void SaveValueToPlayerPrefs(){
		//Debug.Log ("SaveValueToPlayerPrefs "+playerPrefsKey+" "+value);
		PlayerPrefs.SetFloat (playerPrefsKey, value);
		PlayerPrefs.Save ();
	}


	// EVENTS

	void OnValueChange(float val){
	//	Debug.Log ("Scalar OnValueChange: "+val);

		value = val;

		SaveValueToPlayerPrefs ();

		OnChange (value);
	}



}
