﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageHolderScript : MonoBehaviour
{
    public SendImage imageSender;
    public string pathToImage;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void SendMessageToPanel()
    {
        imageSender.TakeImagePath(pathToImage);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
