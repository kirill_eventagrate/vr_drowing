﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendImage : MonoBehaviour
{

    public GameObject shroudPanel;
    private string customerName;
    private string customerEmail;
    private string imagePath;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void SendCustomerData(string name, string email)
    {
        customerName = name;
        customerEmail = email;
    }
    public void TakeImagePath(string path)
    {
        imagePath = path;
    }
    public void SendMessageToEmail()
    {

        EmailComtroller controll = new EmailComtroller();
        //controll.SendImageToEmail(imagePath, customerName, customerEmail);
        gameObject.SetActive(false);
    }
    public void DisableHolder()
    {
        shroudPanel.SetActive(true);
    }
    public void EnableHolder()
    {
        shroudPanel.SetActive(false);
    }
}
