﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageDataScript : MonoBehaviour
{
    public GameObject panel;
    public string imagePath;

    public void OpenPanel()
    {
        panel.SetActive(true);
        panel.GetComponent<SendImage>().DisableHolder();
        panel.GetComponent<SendImage>().TakeImagePath(imagePath);
    }
}
