﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System;

public class ImagesPanelController : MonoBehaviour
{
    public GameObject spritePref;
    public GameObject panel;
    public Transform hoolder;
    public string[] fileNames;
    
    private JObject Settings;
    // Start is called before the first frame update
    void Start()
    {
        FindAllImages();
        SetImageFromFile();
    }
    public void SetImageFromFile()
    {
        int x = 0;
        int y = 0;
        for (int i = 0; i < fileNames.Length; i++)
        {
            Texture2D tex = null;
            byte[] fileData;
            string filePath = "Images\\"+fileNames[i];
            if (File.Exists(filePath))
            {
                fileData = File.ReadAllBytes(filePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);
                Sprite mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                GameObject tempoObj = Instantiate(spritePref, hoolder); 
                tempoObj.GetComponent<Image>().sprite = mySprite;
                tempoObj.GetComponent<ImageDataScript>().imagePath = filePath;
                tempoObj.GetComponent<ImageDataScript>().panel = panel;



                x++;
                if (x > 3)
                {
                    y++;
                    x = 0;
                }
                /*
                sprites[id].sprite = mySprite;
                sprites[id].GetComponent<ImageHolderScript>().pathToImage = filePath;*/
                //sprites[id].preserveAspect = true;
            }

             // xPos is the placement of the components.
        }

    }
    void FindAllImages()
    {
        string path = Path.Combine(Environment.CurrentDirectory + "\\Images");
        fileNames = Directory.EnumerateFiles(path, "*.*")
                           .Where(s => s.EndsWith(".png") || s.EndsWith(".jpg") || s.EndsWith(".bmp"))
                           .Select(Path.GetFileName)
                           .ToArray();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
