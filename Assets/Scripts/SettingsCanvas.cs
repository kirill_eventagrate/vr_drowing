﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Wacki;

public class SettingsCanvas : MonoBehaviour {

    static public SettingsCanvas instance = null;

    public GameObject settingCanvas;
    public GameObject meshContainer;
    public GameObject inputModule;
    public InputField inputField;
    public UndoManager undoManager;
    public JObject Settings;
    public string SettingsFilePath = "Settings.json";

    void Awake()
    {
        if (instance == null)
            instance = this;
        GetSettings(SettingsFilePath);
    }

    // Use this for initialization
    void Start () {
        inputField.text = Settings["PictureSaveFolder"].Value<string>();
        
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            settingCanvas.SetActive(!settingCanvas.activeSelf);
            inputModule.GetComponent<LaserPointerInputModule>().enabled = !inputModule.GetComponent<LaserPointerInputModule>().enabled;
        }
        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            CleanMeshes();
        }
    }

    public void CleanMeshes()
    {
        for (int i = 0; i < meshContainer.transform.childCount; i++)
        {
            Destroy(meshContainer.transform.GetChild(i).gameObject);
        }
        undoManager.ClearData();
    }

    public void ChangeInput(string str)
    {
        PlayerPrefs.SetString("FolderPath", str);
    }

    void GetSettings(string path)
    {
        StreamReader reader = new StreamReader(path, System.Text.Encoding.GetEncoding(1252));
        string text = reader.ReadToEnd();
        reader.Close();
        Settings = JObject.Parse(text);
    }

    public void WriteToExcel(List<object[]> _rowData)
    {
        ExcelImportRunTime.AddExcelRow(_rowData, Settings["ExcelFile"].Value<string>(), Settings["ExcelSheetName"].Value<string>());
    }
}
