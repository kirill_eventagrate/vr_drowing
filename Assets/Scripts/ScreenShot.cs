﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Drawing.Printing;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ScreenShot : MonoBehaviour {
    public SteamVR_TrackedObject trackedObj;
    public GameObject screenShotCamera;
    public GameObject canvas;
    public GameObject screenShotImage;
    public GameObject[] controllers;
    public RenderTexture renderTextureOrigin;

    private Texture2D screenShot;

    public string filename = "";
    public string globalPrinterName = "";

    public GameObject Keyboard;

    public GameObject DataForm;
    public InputField NameInputField;
    public InputField EmailInputField;
    public Button sendButton;
    public EmailComtroller emailComtroller;
    public BrushManager brushManager;

    // Use this for initialization
    void Start () {
        globalPrinterName = SettingsCanvas.instance.Settings["PrinterName"].Value<string>();
    }

    void PrintDrawing()
    {

        Debug.LogWarning("PrintDrawing");


        var installedPrinters = new List<string>();
        foreach (var p in PrinterSettings.InstalledPrinters)
        {
            installedPrinters.Add(p.ToString());
        }

#if UNITY_EDITOR
        var printerName = installedPrinters.Where(x => x.ToLower().Contains(globalPrinterName)).FirstOrDefault();
#else
			var printerName = installedPrinters.Where(x => x.ToLower().Contains(globalPrinterName)).FirstOrDefault();
#endif

        if (printerName == null)
        {
            Debug.LogError("Printer with "+ globalPrinterName + " in name cannot be found.");
            return;
        }




        PrintDocument pd = new PrintDocument();
        pd.PrinterSettings.PrinterName = printerName;


        //var paperSize = new PaperSize("International postcard", 830, 1170);


        /*var resolution = new PrinterResolution();
        resolution.X = 150;
        resolution.Y = 150;*/

        //  pd.DefaultPageSettings.PrinterResolution = resolution;


        //  pd.DefaultPageSettings.Margins = new Margins(20, 20, 20, 3);
        //   pd.OriginAtMargins = true;

        /*pd.DefaultPageSettings.PaperSize = paperSize;
        pd.PrinterSettings.DefaultPageSettings.PaperSize = paperSize;
        */
        pd.DefaultPageSettings.Landscape = true;
        pd.PrinterSettings.DefaultPageSettings.Landscape = true;

        //pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
        //pd.OriginAtMargins = true;

        Debug.Log("Before DrawImageFromFile");

        pd.PrintPage += new PrintPageEventHandler(DrawImageFromFile);

        Debug.Log("before Printer");

        Loom.RunAsync(() =>
        {
            pd.Print();

        });

        Debug.Log("End Printer");


    }

    void DrawImageFromFile(object o, PrintPageEventArgs e)
    {
        System.Drawing.Image i = System.Drawing.Image.FromFile(filename);
        //System.Drawing.Rectangle m = e.MarginBounds;
        //e.Graphics.DrawImage(i, e.MarginBounds);

        // e.Graphics.DrawImage(i, 45,16,327,581);

        //System.Drawing.Rectangle m = new System.Drawing.Rectangle(0, 0, 1122, 793);
        e.Graphics.DrawImage(i, 0,0, 613, 405);

    }


    // Update is called once per frame
    void Update () {
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)trackedObj.index);
        if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger) && BrushManager.canpaint == true)
        {
            Debug.Log("ScreenShot");
            MakeScreenShot();
        }
    }

    public void HideKeyBoard()
    {
        Keyboard.SetActive(false);
        if (NameInputField.text.Length > 0)
        {
            NameInputField.GetComponent<Image>().color = Color.white;
            if (emailComtroller.emailIsCorrect(EmailInputField.text))
            {
                sendButton.gameObject.SetActive(true);
                EmailInputField.GetComponent<Image>().color = Color.white;
            }
            else
            {
                EmailInputField.GetComponent<Image>().color = Color.red;
            }
        }
        else
        {
            NameInputField.GetComponent<Image>().color = Color.red;
        }
        /*if (emailComtroller.emailIsCorrect(EmailInputField.text) && NameInputField.text.Length > 0)
        {
            sendButton.gameObject.SetActive(true);
            EmailInputField.GetComponent<Image>().color = Color.white;
        }else if (!emailComtroller.emailIsCorrect(EmailInputField.text))
        {
            EmailInputField.GetComponent<Image>().color = Color.red;
        }*/
    }

    public void SendPicture()
    {
        if (emailComtroller.emailIsCorrect(EmailInputField.text))
        {
            List<object[]> rowData = new List<object[]>();
            string[] collums = new string[2] { NameInputField.text, EmailInputField.text };
            rowData.Add(collums);
            Loom.RunAsync(() => {
                SettingsCanvas.instance.WriteToExcel(rowData);
            });
            //emailComtroller.SendImageToEmail(filename, NameInputField.text, EmailInputField.text);
            //PrintDrawing();
            HideDataForm();
            ShowCanvas(false);
            
            SettingsCanvas.instance.CleanMeshes();
        }
    }

    public void ShowDataForm()
    {
        NameInputField.GetComponent<Image>().color = Color.white;
        EmailInputField.GetComponent<Image>().color = Color.white;
        sendButton.gameObject.SetActive(false);
        DataForm.SetActive(true);
    }

    public void HideDataForm()
    {
        DataForm.SetActive(false);
        NameInputField.text = "";
        EmailInputField.text = "";
        sendButton.gameObject.SetActive(false);
        brushManager.ShowBrushesPanel();
        screenShotImage.GetComponent<RawImage>().texture = renderTextureOrigin;
    }

    private void MakeScreenShot()
    {
        controllers[0].SetActive(false);
        controllers[1].SetActive(false);
        int width = 1920;//mainCamera.GetComponent<Camera>().pixelWidth;
        int height = 1080;//mainCamera.GetComponent<Camera>().pixelHeight;


        RenderTexture rt = new RenderTexture(width, height, 24);
        //screenShotCamera.GetComponent<Camera>().targetTexture = rt;
        screenShot = new Texture2D(width, height);
        //screenShotCamera.GetComponent<Camera>().Render();
        RenderTexture.active = renderTextureOrigin;
        screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0); // rawImage

        Texture2D logoTex = null;
        if (File.Exists(SettingsCanvas.instance.Settings["LOGO"].Value<string>()))   // "SavePicture/cicrcle.png"
        {
            byte[] fileData;
            fileData = File.ReadAllBytes(SettingsCanvas.instance.Settings["LOGO"].Value<string>());
            logoTex = new Texture2D(2, 2);
            logoTex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }

        for (int x = 0; x < (int)screenShot.width; x++)
        {
            for (int y = 0; y < (int)screenShot.height; y++)
            {
                if (x > (screenShot.width - logoTex.width) && y > (screenShot.height - logoTex.height))
                {
                    Color backgroundColor = screenShot.GetPixel(x, y);
                    Color color = logoTex.GetPixel(x - (screenShot.width - logoTex.width), y - (screenShot.height - logoTex.height));
                    

                    Color combined =   Color.Lerp(backgroundColor, color, color.a);
                    screenShot.SetPixel(x, y, combined);

                }
            }
        }
        //screenShot.
        screenShot.Apply();
        //screenShotCamera.GetComponent<Camera>().targetTexture = null;
        RenderTexture.active = null;
        Destroy(rt);

        screenShotImage.GetComponent<RawImage>().texture = screenShot;
        //screenShotImage.GetComponent<AspectRatioFitter>().aspectRatio = (float)(width / height);
        controllers[0].SetActive(true);
        controllers[1].SetActive(true);


    }
    public void SaveScreenShot()
    {
        if (screenShot != null)
        {
            if (PlayerPrefs.HasKey("FolderPath") && PlayerPrefs.GetString("FolderPath") != "")
            {
                byte[] bytes = screenShot.EncodeToPNG();

                System.DateTime thisDay = System.DateTime.Now;
                filename = SettingsCanvas.instance.Settings["PictureSaveFolder"].Value<string>() + "screenShot-" + thisDay.Month + "-" + thisDay.Day + "-" + thisDay.Year + "-" + thisDay.Hour + "-" + thisDay.Minute + "-" + thisDay.Second + ".png";
                //Debug.Log(filename);

                try
                {
                    File.WriteAllBytes(filename, bytes);
                    Debug.Log("File created");
                    PrintDrawing();
                    SettingsCanvas.instance.CleanMeshes();
                    brushManager.CylindarBrushtoggle();
                }
                catch (IOException ex)
                {
                    //Debug.Log("Exception caught in process: {0}", ex);
                    Debug.Log("File not created");
                }
            }
        }
    }
    public void ShowCanvas(bool state)
    {
        canvas.SetActive(state);
        Keyboard.SetActive(state);
        screenShotImage.GetComponent<RawImage>().texture = renderTextureOrigin;
    }
}
