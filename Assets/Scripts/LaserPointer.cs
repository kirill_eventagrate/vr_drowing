﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointer : MonoBehaviour {

    private SteamVR_TrackedObject trackedObj;

    // 1
    public GameObject laserPrefab;
    // 2
    private GameObject laser;
    // 3
    private Transform laserTransform;
    // 4
    private Vector3 hitPoint;



   
    public Transform cameraRigTransform;
   
    public GameObject teleportReticlePrefab;
    // 3
    private GameObject reticle;
    // 4
    private Transform teleportReticleTransform;
    // 5
    public Transform headTransform;
    // 6
    public Vector3 teleportReticleOffset;
    // 7
    public LayerMask teleportMask;
    // 8
    private bool shouldTeleport;


    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Use this for initialization
    void Start () {

     
        laser = Instantiate(laserPrefab);
       
        laserTransform = laser.transform;

       
        reticle = Instantiate(teleportReticlePrefab);
       
        teleportReticleTransform = reticle.transform;

    }


    private void ShowLaser(RaycastHit hit)
    {
       
        laser.SetActive(true);
       
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
        
        laserTransform.LookAt(hitPoint);
        
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance);
    }




    // Update is called once per frame
    void Update () {
        
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Grip))
        {
            RaycastHit hit;

         
            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 500, teleportMask))
            {
                hitPoint = hit.point;
                ShowLaser(hit);
                Debug.Log("Im Reaching here");
                
                reticle.SetActive(true);
               
                teleportReticleTransform.position = hitPoint + teleportReticleOffset;
              
                shouldTeleport = true;
            }
        }
        else 
        {
            laser.SetActive(false);
            reticle.SetActive(false);

        }

        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && shouldTeleport)
        {
            Teleport();
        }

    }


    private void Teleport()
    {
       
        shouldTeleport = false;
        
        reticle.SetActive(false);
       
        Vector3 difference = cameraRigTransform.position - headTransform.position;
        
        difference.y = 0;
      
        cameraRigTransform.position = hitPoint + difference;
    }
}
