﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ColorManager : MonoBehaviour {
	public static ColorManager Instance;
	public ColorPicker picker;
	public Color col;

    public delegate void ClickAction();
    public static event ClickAction OnClicked;


    void Awake(){

		if (Instance == null) {

			Instance = this;

		}
	}
	void OnDestroy(){
		if (Instance ==this) {

			Instance = null;

		}
        
	}

	private Color color;
	
	public void OnColorChange(){

        col =  picker.CurrentColor;
        OnClicked();

    }
    public Color GetCurrentColor(){

		return col; 

	}
}
