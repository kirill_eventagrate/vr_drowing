﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Mail;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public class EmailComtroller: MonoBehaviour
{
    public void SendImageToEmail(string pathToPicture, string name, string toAdress)
    {
        Loom.RunAsync(() => {
            string EmailAdressFrom = SettingsCanvas.instance.Settings["EmailAdressFrom"].Value<string>(); // "info@eventagrate.com"
            string EmailNameFrom = SettingsCanvas.instance.Settings["EmailNameFrom"].Value<string>(); // "Eventagrate"
            MailAddress from = new MailAddress(EmailAdressFrom, EmailNameFrom);
            MailAddress to = new MailAddress(toAdress);

            // создаем объект сообщения
            MailMessage m = new MailMessage(from, to);

            m.Subject = SettingsCanvas.instance.Settings["Message_Subject"].Value<string>(); // "Requested image";

            
            string EmailBody = SettingsCanvas.instance.Settings["Message_Body"].Value<string>().Replace("{name}", name);
            m.Body = EmailBody;  // "<h2>Hello " + name + ". This is your screenshot.</h2>";
                                 // письмо представляет код html
            m.IsBodyHtml = true;
            // адрес smtp-сервера и порт, с которого будем отправлять письмо

            m.Attachments.Add(new Attachment(pathToPicture));
            //smtp сервер 

            string smtpHost = SettingsCanvas.instance.Settings["SmtpHost"].Value<string>();  // "smtp.sendgrid.net";
                                                                                             //smtp порт 
            int smtpPort = SettingsCanvas.instance.Settings["SmtpPort"].Value<int>();  // 587;
                                                                                       //логин 
            string login = SettingsCanvas.instance.Settings["SMTP_login"].Value<string>();  // "eventagrate";
                                                                                            //пароль 
            string pass = SettingsCanvas.instance.Settings["SMTP_password"].Value<string>();  // "Eventagrate1";
            Debug.Log(smtpHost + "; " + smtpPort + "; " + login + "; " + pass);
            SmtpClient smtp = new SmtpClient(smtpHost, smtpPort);
            smtp.Credentials = ((System.Net.ICredentialsByHost)new NetworkCredential(login, pass));
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            { return true; };
            try
            {
                smtp.Send(m);
            }
            catch (System.Exception ex)
            {
                Debug.Log("Сообщение не отправленно! " + ex);
            }
        });
        
    }

    public bool emailIsCorrect(string email)
    {
        if (new EmailAddressAttribute().IsValid(email))
            return true;
        return false;
    }
}
/*
 
     "SmtpHost": "smtp.sendgrid.net",
  "SmtpPort": 587,
  "SMTP_login": "eventagrate",
  "SMTP_password": "Eventagrate1"
     
     */
