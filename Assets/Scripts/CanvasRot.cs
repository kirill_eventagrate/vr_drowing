﻿using UnityEngine;
using System.Collections;
using Valve.VR.InteractionSystem;

public class CanvasRot : MonoBehaviour {


	#if UNITY_STANDALONE_WIN 
	public SteamVR_TrackedObject controller;
    public Hand hand;


    public float speed;

	#elif UNITY_ANDROID 




	#endif

	// Use this for initialization
	public float currentXAxis;
	public float lateXAxis;

	void Start () {
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)controller.index);
        currentXAxis = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis1)[0] + 1;
    }

	// Update is called once per frame
	void Update () {
#if UNITY_STANDALONE_WIN
        
        //		currentXAxis = Input.GetTouch(0).deltaPosition.magnitude;

            transform.Rotate(Vector3.forward * (currentXAxis - lateXAxis )* speed * Time.deltaTime);
 
		#endif
	
	}
	void LateUpdate () {

		#if UNITY_STANDALONE_WIN 
		SteamVR_Controller.Device device = SteamVR_Controller.Input((int)controller.index);


		lateXAxis = device.GetAxis (Valve.VR.EVRButtonId.k_EButton_Axis0) [0]+1;

		#endif
	}
}
