﻿using HeathenEngineering.UIX;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldSwitcher : MonoBehaviour {

	public Keyboard keyboard;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnClick()
	{
		keyboard.gameObject.SetActive(true);
		keyboard.linkedGameObject = gameObject;
		keyboard.linkedBehaviour = gameObject.GetComponent<InputField>();
		keyboard.linkedBehaviours.Clear();
		keyboard.linkedBehaviours.Add(gameObject.GetComponent<RectTransform>());
		keyboard.linkedBehaviours.Add(gameObject.GetComponent<CanvasRenderer>());
		keyboard.linkedBehaviours.Add(gameObject.GetComponent<Image>());
		keyboard.linkedBehaviours.Add(gameObject.GetComponent<InputField>());
		keyboard.linkedBehaviours.Add(gameObject.GetComponent<LigatureHelper>());
	}
}
