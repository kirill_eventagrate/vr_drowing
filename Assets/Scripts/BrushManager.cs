﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Wacki;

public class BrushManager : MonoBehaviour {
	public static bool canpaint;
	public static bool couldpaint;

	#if UNITY_STANDALONE_WIN 


	public SteamVR_TrackedObject controller;


	#elif UNITY_ANDROID 



	public static bool TangoPainting;
	public static bool TangoPaintingStart;
	public Slider cursorSizer;
	#endif

	public  Text btex;
	// painted bool that switches between the two brushes
	//will change to case sw when added a third brush
	public static int brushswitchint;
	public GameObject con;
	public static bool canRetexture;
	public static bool couldRetexture;

	public GameObject cursor;
    
	public Text LineText;
	public static bool freeformbool;
	public GameObject lineBreak;
	public static float cursorsize;
	public Button squarebrush; 
	public Button cylandarbrush;
    public Button screenShotButton;
    public Button saveButton;

	public InputField NameInputField;
	public InputField EmailInputField;
	public Button sendButton;

	public Image flatbrush;
	public Image retextureIMG;
	public ToolManager tooler;
	public ScreenShot ScreenShotComponent;

	public GameObject CanvasPanel;
	public GameObject UserDataFormPanel;
	public GameObject ViveController;

	
	// Use this for initialization
	void Start ()
    {

	#if UNITY_ANDROID 
		cursorsize=.05f;
		sliderValue();

		canpaint = true;
		#endif
		//need to make this a singlton 
		cursorsize =  0.05f;

		CylindarBrushtoggle ();
	
		flatbrushtoggle ();
		CylindarBrushtoggle ();
		cursor.transform.localScale = new Vector3 (cursorsize, cursorsize, cursorsize);
		
	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_STANDALONE_WIN

		if (controller.gameObject.activeSelf)
		{
			SteamVR_Controller.Device device = SteamVR_Controller.Input((int)controller.index);

			if (device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad))
			{
				cursorsize = (device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0)[0] + 1) * 0.07f;
				cursor.transform.localScale = new Vector3(cursorsize, cursorsize, cursorsize);
			}
		}
		

#endif
	}

		#if UNITY_ANDROID 

	public void sliderValue(){
		cursorsize = cursorSizer.value;




		cursor.transform.localScale = new Vector3 (cursorsize, cursorsize, cursorsize);
	}
		#endif

	
	public void ShowUserDataPanel()
	{
		CanvasPanel.SetActive(false);
		ViveController.GetComponent<ViveUILaserPointer>().enabled = true;
		//UserDataFormPanel.SetActive(true);
		ScreenShotComponent.ShowDataForm();
	}

	public void ShowBrushesPanel()
	{
		CanvasPanel.SetActive(true);
		ViveController.GetComponent<ViveUILaserPointer>().enabled = false;
		//UserDataFormPanel.SetActive(false);
	}

	

	public void CylindarBrushtoggle()
    {
	
		if (freeformbool == true) {
			con.GetComponent<PinchDraw> ().pointerbreaker ();
			con.GetComponent<dLineManager> ().pointbreaker ();
		}
		brushswitchint = 0;
        con.GetComponent<ScreenShot>().ShowCanvas(false);
        con.GetComponent<ScreenShot>().enabled = false;
        con.GetComponent<dLineManager> ().enabled = false;
		con.GetComponent<PinchDraw> ().enabled = true;
        squarebrush.interactable = false;
        cylandarbrush.interactable = true;
        screenShotButton.interactable = true;
        saveButton.interactable = false;

        canpaint = true;
		//tooler.teleportoff();
	}

	public void flatbrushtoggle(){
	
	
		brushswitchint = 1;
		if (freeformbool == true) {
			con.GetComponent<PinchDraw> ().pointerbreaker ();
			con.GetComponent<dLineManager> ().pointbreaker ();
		}

        squarebrush.interactable = true;
        cylandarbrush.interactable = false;
        screenShotButton.interactable = true;
        saveButton.interactable = false;

        con.GetComponent<ScreenShot>().ShowCanvas(false);
        con.GetComponent<ScreenShot>().enabled = false;
        con.GetComponent<dLineManager>().enabled = true;
		con.GetComponent<PinchDraw>().enabled =false;
		canpaint = true;

		//tooler.teleportoff();
	}

    public void ScreenShotToggle()
    {
        cylandarbrush.interactable = true;
        squarebrush.interactable = true;
        screenShotButton.interactable = false;
        saveButton.interactable = true;

        con.GetComponent<ScreenShot>().ShowCanvas(true);
        con.GetComponent<ScreenShot>().enabled = true;
        con.GetComponent<dLineManager>().enabled = false;
        con.GetComponent<PinchDraw>().enabled = false;
    }

    public  void squarebrushtoggle(){
	
	
		brushswitchint = 2;
	//	cylandarbrush.color =Color.white; 
		//squarebrush.color = Color.gray;
		flatbrush.color = Color.white;
		con.GetComponent<dLineManager> ().enabled =true;
		con.GetComponent<PinchDraw> ().enabled =false;

		con.GetComponent<PinchDraw> ().pointerbreaker ();
		con.GetComponent<dLineManager> ().pointbreaker ();
		canpaint = true;

		//tooler.teleportoff();
	}

	public void deselectAllbrushes(){
		flatbrush.color = Color.white;
	//	cylandarbrush.color =Color.white; 
		//squarebrush.color = Color.white;
		con.GetComponent<PinchDraw> ().enabled =false;
		con.GetComponent<dLineManager> ().enabled = false;
	}




	public void LineChoice(){

		freeformbool = !freeformbool;

		if (freeformbool == true) {

			LineText.text = "Line Type: Point Build";
			lineBreak.SetActive (true);
			if (con.GetComponent<PinchDraw> ().firstpointtime == true) {
				
				con.GetComponent<PinchDraw> ().pointerbreaker ();
			
			}
            

		} else {
			LineText.text = "Line Type: Freeform";
			lineBreak.SetActive (false);
			if (con.GetComponent<dLineManager> ().firstPointtime == true) {

				con.GetComponent<dLineManager> ().pointbreaker ();
                
			}
		}



	}

	public void retexture(){


		canRetexture = !canRetexture;
		couldRetexture = !couldRetexture;

		if (canRetexture == true) {
		
			retextureIMG.color = Color.grey;
			canpaint = false;
			cursor.SetActive (false); 
			tooler.laser.SetActive (true);

		} else {
			

			cursor.SetActive (true); 
			tooler.laser.SetActive (false);
			retextureIMG.color = Color.white;

			//if (ToolManager.couldteleport == true) {
			//	tooler.telporton ();

			} //else {
				
				canpaint = true; 
				if (brushswitchint ==0) {
					CylindarBrushtoggle ();

				} else if  (brushswitchint ==0) {

					flatbrushtoggle ();
				}
		//	} 
		

		}


	}

	

	#if UNITY_ANDROID 



	public void PaintingStart(){
		TangoPaintingStart = true; 
		TangoPainting = true;
		Debug.Log ("PaintingStartt");
	} 

	public void PaintingEnd(){



		TangoPainting = false;
		Debug.Log ("End");
	}

	#endif


