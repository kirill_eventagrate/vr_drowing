﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class EnivormentManager : MonoBehaviour {
	
	public Material[] Skyboxes; 
	public int skyboxint= 0;
    public GameObject[] environments;
//	public Text enviormentText;

    public Button[] skyboxBtns;



    void Start()
    {
        skyboxint = 0;
        SkyboxSwitch();
    }

    
    // Button Event
    public void OnSkyBoxChanged(int index)
    {
        skyboxint = index;
        SkyboxSwitch();
    }

    
    public void OnSkyBoxButtonPressed(GameObject btn)
    {
        EnableAllButtons();
        btn.GetComponent<Button>().interactable = false;
       
    }

    // METHODS
    
    private void EnableAllButtons()
    {
        for (int i = 0; i < skyboxBtns.Length; i++)
        {
            skyboxBtns[i].interactable = true;
        }
    }


    public void SkyboxSwitch()
	{
        
        switch (skyboxint)
			{
            case 3:
                environments[0].SetActive(false);
                environments[1].SetActive(false);
                environments[2].SetActive(false);
                environments[3].SetActive(true);
                RenderSettings.skybox = Skyboxes[1];
               // enviormentText.text = "Full Moon";
                Debug.Log(skyboxint);
                break;
            case 2:
                environments[0].SetActive(false);
                environments[1].SetActive(false);
                environments[2].SetActive(true);
                environments[3].SetActive(false);
                RenderSettings.skybox = Skyboxes [1];
			//enviormentText.text = "Low Stars";
                Debug.Log(skyboxint);
				break;
			case 1:
                environments[0].SetActive(false);
                environments[1].SetActive(true);
                environments[2].SetActive(false);
                environments[3].SetActive(false);
                RenderSettings.skybox = Skyboxes [1];
			//enviormentText.text = "Full Night";
                Debug.Log(skyboxint);

                break;
			case 0:
                environments[0].SetActive(true);
                environments[1].SetActive(false);
                environments[2].SetActive(false);
                environments[3].SetActive(false);
                RenderSettings.skybox = Skyboxes [0];
			//enviormentText.text = "Blue Nebular";
                Debug.Log(skyboxint);
                break;
		default:
                Debug.LogWarning("UnKown Skybox");
                Debug.Log("skyboxint: " + skyboxint);
                break;
				
			}
		}

	
}
