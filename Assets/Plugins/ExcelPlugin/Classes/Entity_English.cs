using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_English : ScriptableObject
{	
	public List<Sheet> sheets = new List<Sheet> ();

    [System.SerializableAttribute]
	public class Sheet
	{
		public string name = string.Empty;
		public List<ProjectParam> TextValueslist = new List<ProjectParam>();
    }

    [System.SerializableAttribute]
	public class ProjectParam
	{
		//public string Text_id;
		public string Name;
		public string Value;
	}

    public ProjectParam GetDataById(int _id)
    {
        return sheets[0].TextValueslist[_id - 2];
    }
}

public enum ExcelParam {
    //Text_id,
    Name,
    Value
}

